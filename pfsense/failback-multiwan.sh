#!/bin/sh
# This script was lifted from https://www.reddit.com/r/PFSENSE/comments/l1ssrb/dual_wan_failover_failback_on_recovery/

# *** kills firewall states on Tier2 (Mobile Data failover) when Tier1 WAN is up ***

# Static vars
TIER1_IF="bge0"
TIER1_GW="WAN_GW"
TIER2_IF="bge1"
TIER2_NSTATES_BASELINE=1

# Dynamic vars
CURRENT_TIME="$(date +"%c")"
DEFAULT_IF=$(route -n show default | awk '/interface/{print $2}')
TIER1_STATUS="$(pfSsh.php playback gatewaystatus brief | grep "$TIER1_GW" | awk '{print $2}')"

# Only check for lingering Tier2 states and clear them when Tier1 is default and gateway is fine
if [ "$DEFAULT_IF" == "$TIER1_IF" ] && [ "$TIER1_STATUS" == "none" ]; then
    # Check current number of Tier2 states
    TIER2_NSTATES_CURRENT=$(pfctl -s state | grep "$TIER2_IF" | wc -l)

    # If number of states on Backup is above the baseline, then flush Backup Interface states
    if [ $TIER2_NSTATES_CURRENT -gt $TIER2_NSTATES_BASELINE ]; then
        echo "$CURRENT_TIME: Primary online, but some connections on Backup. Flushing Backup states"
        pfctl -i $TIER2_IF -F states
    fi
fi